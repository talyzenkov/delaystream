<?

namespace Tsv\DelayStream;

IncludeModuleLangFile(__FILE__);

\CJSCore::Init(['jquery', 'date']);
\Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/js/tsv.delaystream/integration.js');
$arIntegration = [];
$arIntegration['placeholder'] = Main::getMessage('INPUT_DATE_PUBLISH_PLACEHOLDER');
$script = '
                <script type="text/javascript">
                    BX.ready(function(){
                        var tsvB24streamVar = new tsvB24stream(' . \CUtil::PhpToJSObject($arIntegration, false, true) . ');
                    });
                </script>
            ';
\Bitrix\Main\Page\Asset::getInstance()->addString($script);

Class Main {

   static $MODULE_ID = 'tsv.delaystream';
   static $MODULE_CODE = 'TSV_DELAYSTREAM';
   static $NAMESPACE = '\Tsv\DelayStream';

   static function getMessage($strLangKey, $arReplace = []) {

      $strPhrase = static::$MODULE_CODE . '_' . $strLangKey;
      $strMessage = \Bitrix\Main\Localization\Loc::getMessage($strPhrase, $arReplace);
      $strMessage = str_replace('#LANG_ID#', LANGUAGE_ID, $strMessage);
      if (empty($strMessage)) {
         $strMessage = '#' . $strLangKey . '#';
      }
      return $strMessage;
   }

}

?>