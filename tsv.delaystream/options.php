<?
if (!$USER->IsAdmin())
	return;

IncludeModuleLangFile(__FILE__);

$aTabs = array(
	array("DIV" => "edit1", "TAB" => GetMessage("TSV_LOG_MAIN_TAB_SET"), "ICON" => "ib_settings", "TITLE" => GetMessage("TSV_LOG_MAIN_TAB_TITLE_SET")),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);
$arCheckboxOptions = array('DISABLED'/* , 'AGENT_CLEAN_LOG' */, 'BY_DAY', 'UID', 'HIDE_TIME', 'HIDE_DATE', 'USE_PP', 'ONLY_MY_IP');
if ($REQUEST_METHOD == "POST" && strlen($Update) > 0 && check_bitrix_sessid())
{
	// SKEEP SESSION CHECK IN log FUNCTION
	$_SESSION['TSV_LOG_ONLY_MY_IP'] = '';
	$_SESSION['TSV_LOG_DISABLED'] = '';

	// UNSET OPTION
	foreach ($arCheckboxOptions as $v)
	{
		if (!$_POST['FIELDS'][$v])
			$_POST['FIELDS'][$v] = 'N';
	}

	// SET OPTION
	foreach ($_POST['FIELDS'] as $key => $val)
	{
		if (in_array($key, $arCheckboxOptions) && $val != 'Y')
			$val = 'N';

		COption::SetOptionString(tsv_log::MODULE_ID, $key, $val);
	}
}

$tabControl->Begin();
?>
<style>
	input[type=text]{
		width: 200px;
	}
</style>
<form method="post" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($mid) ?>&amp;lang=<? echo LANGUAGE_ID ?>">
	<? $tabControl->BeginNextTab(); ?>
	<tr class="heading">
		<td align="center" valign="top" colspan="3"><?= GetMessage("TSV_LOG_MAIN_TAB_TITLE"); ?></td>
	</tr>
	<tr >
		<td colspan="3">
			<p>
				<?= GetMessage("TSV_LOG_EXAMPLE"); ?>
			</p>
		</td>
	</tr>
	<?
	foreach ($arCheckboxOptions as $code)
	{
		?>
		<tr>
			<td valign="top" style="text-align: left" colspan="3">
				<? $checked = (COption::GetOptionString(tsv_log::MODULE_ID, $code) == "Y") ? 'checked="checked"' : ''; ?>
				<label ><input type="checkbox" value="Y" name="FIELDS[<?= $code ?>]" <?= $checked ?>><?= GetMessage("TSV_LOG_" . $code); ?></label>
			</td>

		</tr>
	<? }
	?>

	<tr>
		<td valign="top" style="text-align: left">
			<input type="text" id="MY_IP" name="FIELDS[MY_IP]" value="<?= COption::GetOptionString(tsv_log::MODULE_ID, "MY_IP") ?>">
			<label for="MY_IP"><?= GetMessage("TSV_LOG_MY_IP"); ?></label>
			<p><?= '$_SERVER[REMOTE_ADDR] ' . $_SERVER['REMOTE_ADDR']; ?></p>

		</td>
		<td valign="middle">&nbsp;</td><td>&nbsp;</td>
	</tr>

	<tr>
		<td valign="top" style="text-align: left">
			<input type="text" value="<?= COption::GetOptionString(tsv_log::MODULE_ID, "LOG_VIEW_SIZE") ?>" name="FIELDS[LOG_VIEW_SIZE]">
			<label for="LOG_VIEW_SIZE"><?= GetMessage("TSV_LOG_VIEW_SIZE"); ?></label>
		</td>
		<td valign="middle">

		</td>
		<td>
			&nbsp;
		</td>
	</tr>

	<? /* todo/?>
	  <tr>
	  <td valign="top" style="text-align: left">
	  <? $checked = (COption::GetOptionString(tsv_log::MODULE_ID, "AGENT_CLEAN_LOG") == "Y") ? 'checked="checked"' : ''; ?>
	  <input type="checkbox" value="Y" id="AGENT_CLEAN_LOG" name="FIELDS[AGENT_CLEAN_LOG]" <?= $checked ?>>
	  <label for="AGENT_CLEAN_LOG"><?= GetMessage("TSV_LOG_AGENT_CLEAN_LOG"); ?></label>
	  </td>
	  <td valign="middle">

	  </td>
	  <td>
	  &nbsp;
	  </td>
	  </tr>
	  <?/* */ ?>



	<? $tabControl->Buttons(); ?>
	<input type="submit" name="Update" value="<?= GetMessage("TSV_LOG_MAIN_SAVE") ?>" title="<?= GetMessage("TSV_LOG_MAIN_OPT_SAVE_TITLE") ?>">
	<?= bitrix_sessid_post(); ?>
	<? $tabControl->End(); ?>
</form>
