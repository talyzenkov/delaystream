<?php

namespace Tsv\DelayStream;

class Agents {

   /**
    * Агент активирует пост<br>
    *
    * @param int $postId
    * */
   function activateBlogPost($postId) {
      \Bitrix\Main\Loader::includeModule('blog');
      $post = \CBlogPost::GetByID(intval($postId));
      if ($post) {
         \CBlogPost::Update($post['ID'], ["PUBLISH_STATUS" => BLOG_PUBLISH_STATUS_PUBLISH]);
         \CAgent::RemoveAgent("\Tsv\DelayStream\Agents::activateBlogPost('" . $postId . "');", "tsv.delaystream");
      }
   }

}
