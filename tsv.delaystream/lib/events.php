<?php

namespace Tsv\DelayStream;

class Events {

   function OnBeforePostAdd(&$fields) {
      $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
      if ($request->getPost("DELAY_TIME")) {
         $fields['DATE_PUBLISH'] = $request->getPost("DELAY_TIME");
         $fields['PUBLISH_STATUS'] = 'K';
      }
   }

   function OnPostAdd($postId, $fields) {
      $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
      if ($request->getPost("DELAY_TIME")) {

         \CAgent::AddAgent(
                 "\Tsv\DelayStream\Agents::activateBlogPost('" . $postId . "');", // имя функции
                 "tsv.delaystream",
                 "Y", // агент не критичен к кол-ву запусков
                 86400, // интервал запуска - 1 сутки
                 $fields['DATE_PUBLISH'], // дата первой проверки на запуск
                 "Y", // агент активен
                 $fields['DATE_PUBLISH'], // дата первого запуска
                 30);
      }
   }

}
