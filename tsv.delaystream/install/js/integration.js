function slog(str) {
   console.log(str);
}
;
(function (window) {
   'use strict';

   if (window.tsvB24stream)
      return;
   window.tsvB24stream = function (arParams) {
      if (typeof arParams === 'object') {
         this.Params = arParams;
      }
      this.init();
   };
   window.tsvB24stream.prototype = {
      init: function () {
         slog('tsvB24stream init()');
         var target = $('#fulloPostFormLHE_blogPostForm')[0];
         var arParams = this.Params;
         if ($('#fulloPostFormLHE_blogPostForm').length)
         {
            var observer = new MutationObserver(function (mutations) {
               mutations.forEach(function (mutation) {
                  if (mutation.type == 'childList') {
                     input = BX.create('INPUT', {
                        attrs: {
                           type: 'text',
                           id: 'DELAY_TIME',
                           name: 'DELAY_TIME',
                           value: '',
                           class: 'task-date webform-field-action-link',
                           placeholder: arParams['placeholder'],

                        },
                        style: {
                           'border': 'none',
                           'border-bottom': '1px dotted grey',
                        }
                     });


                     BX('bx-mention-blogPostForm-id').appendChild(input);
                     BX.bind(BX('DELAY_TIME'), 'click', function () {
                        BX.calendar({node: this, field: this, bTime: true})
                     })
                  }
               });
            });

            var config = {attributes: true, childList: true, characterData: true};

            observer.observe(target, config);

            //observer.disconnect();




         }
      }

   };
})(window);
