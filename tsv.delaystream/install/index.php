<?

IncludeModuleLangFile(__FILE__);

Class tsv_delaystream extends CModule {

   const MODULE_ID = 'tsv.delaystream';

   var $MODULE_ID = 'tsv.delaystream';
   var $MODULE_VERSION;
   var $MODULE_VERSION_DATE;
   var $MODULE_NAME;
   var $MODULE_DESCRIPTION;
   var $MODULE_GROUP_RIGHTS = "Y";
   static $MODULE_CODE = 'TSV_DELAYSTREAM';
   static $NAMESPACE = '\Tsv\DelayStream';
   static $BITRIX_PATH = '';
   static $MODULE_PATH = '';
   static $COPY_DIRS = ['js']; //'tools', 'images', 'js', 'themes'
   static $EVENTS = ['blog' => 'OnBeforePostAdd', 'blog' => 'OnPostAdd',];

   function __construct() {
      $arModuleVersion = array();
      include(dirname(__FILE__) . "/version.php");
      $this->MODULE_VERSION = $arModuleVersion["VERSION"];
      $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
      $this->MODULE_NAME = self::getMessage("MODULE_NAME");
      $this->MODULE_DESCRIPTION = self::getMessage("MODULE_DESCRIPTION");

      $this->PARTNER_NAME = self::getMessage("PARTNER_NAME");
      $this->PARTNER_URI = 'https://tsv.rivne.me';

      self::$MODULE_PATH = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . self::MODULE_ID . '/';
      if (!file_exists(self::$MODULE_PATH) && file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . self::MODULE_ID . '/'))
         self::$MODULE_PATH = $_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . self::MODULE_ID . '/';
      self::$BITRIX_PATH = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/';
   }

   function InstallDB($arParams = array()) {
      global $DB, $DBType;
      //$DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/'.self::MODULE_ID.'/install/db/'.strtolower($DBType).'/install.sql');

      return true;
   }

   function UnInstallDB($arParams = array()) {
      global $DB, $DBType;
      if ($_REQUEST['savedata'] != 'Y') {
         //$DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/'.self::MODULE_ID.'/install/db/'.strtolower($DBType).'/uninstall.sql');
      }

      return true;
   }

   function InstallFiles($arParams = array()) {

      foreach (self::$COPY_DIRS as $dirName) {
         CopyDirFiles(
                 $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . $this->MODULE_ID . '/install/' . $dirName,
                 $_SERVER['DOCUMENT_ROOT'] . '/bitrix/' . $dirName, true, true
         );
      }
      if (is_dir($p = self::$MODULE_PATH . 'admin')) {
         if ($dir = opendir($p)) {
            while (false !== $item = readdir($dir)) {
               if ($item == '..' || $item == '.' || $item == 'menu.php')
                  continue;

               file_put_contents($file = self::$BITRIX_PATH . 'admin/' . self::MODULE_ID . '_' . $item, '<' . '? require("' . self::$MODULE_PATH . 'admin/' . $item . '");?' . '>');
            }
            closedir($dir);
         }
      }

      return true;
   }

   function UnInstallFiles() {
      if (is_dir($p = self::$MODULE_PATH . 'admin')) {
         if ($dir = opendir($p)) {
            while (false !== $item = readdir($dir)) {
               if ($item == '..' || $item == '.')
                  continue;
               unlink(self::$BITRIX_PATH . 'admin/' . self::MODULE_ID . '_' . $item);
            }
            closedir($dir);
         }
      }

      return true;
   }

   function InstallEvents() {
      if (count(self::$EVENTS)) {
         foreach (self::$EVENTS as $module => $eventName) {
            \Bitrix\Main\EventManager::getInstance()->registerEventHandler(
                    $module,
                    $eventName,
                    $this->MODULE_ID,
                    self::$NAMESPACE . '\Events',
                    $eventName
            );
         }
      }


      return true;
   }

   function DoInstall() {
      global $APPLICATION;

      if ($GLOBALS['APPLICATION']->GetGroupRight('main') < 'W') {
         $APPLICATION->ThrowException(self::getMessage("NO_RIGHTS_TO_INSTALL"));
         return;
      }


      $this->InstallEvents();
      $this->InstallFiles();
      $this->InstallDB();
      RegisterModule(self::MODULE_ID);
   }

   function DoUninstall() {
      global $APPLICATION;

      if ($GLOBALS['APPLICATION']->GetGroupRight('main') < 'W') {
         $APPLICATION->ThrowException(self::getMessage("NO_RIGHTS_TO_UNINSTALL"));
         return;
      }

      $this->UnInstallFiles();
      $this->UnInstallDB();
      UnRegisterModule($this->MODULE_ID);
   }

   static function getMessage($strLangKey, $arReplace = []) {

      $strPhrase = static::$MODULE_CODE . '_' . $strLangKey;
      $strMessage = \Bitrix\Main\Localization\Loc::getMessage($strPhrase, $arReplace);
      $strMessage = str_replace('#LANG_ID#', LANGUAGE_ID, $strMessage);
      if (empty($strMessage)) {
         $strMessage = '#' . $strLangKey . '#';
      }
      return $strMessage;
   }

}

?>
