<?

$messPre = 'TSV_DELAYSTREAM_';
$MESS[$messPre . 'MODULE_NAME'] = 'Отложенная публикация в живой ленте';
$MESS[$messPre . 'MODULE_DESCRIPTION'] = 'Позволяет возможность отложенной публикации сообщений из Живой Ленты и из личной страницы профиля';
$MESS[$messPre . 'PARTNER_NAME'] = 'Талызенков Сергей';
$MESS[$messPre . 'NO_RIGHTS_TO_INSTALL'] = 'Нет прав на установку';
$MESS[$messPre . 'NO_RIGHTS_TO_UNINSTALL'] = 'Нет прав на деинсталяцию';
?>